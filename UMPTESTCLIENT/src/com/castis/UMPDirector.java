package com.castis;

import com.castis.Player.VODPlayManager;
import com.castis.Player.VODPlayerImpl;
import com.castis.manager.STBInfoManager;
import com.castis.util.Logger;
import com.castis.vod.control.VODPlayer;
import org.dvb.io.ixc.IxcRegistry;

import javax.tv.xlet.XletContext;
import java.rmi.AlreadyBoundException;

/**
 * Created by forest on 2017-10-24.
 */
public class UMPDirector {

    public UMPDirector(XletContext xc) {

        STBInfoManager.getInstance().setPwsIp("10.9.50.7");
        STBInfoManager.getInstance().setPwsPort("8085");

        Logger.println(this, STBInfoManager.getInstance().getPwsIp());
        Logger.println(this, STBInfoManager.getInstance().getPwsPort());
        try {
            Logger.println(this, "VOD binding");
            IxcRegistry.bind(xc, VODPlayer.RMI_APP_NAME, new VODPlayerImpl(new VODPlayManager()));
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
            Logger.println(Logger.ERROR, this, "Bind Fail");
            Logger.println(this, e.getMessage());
        }
        if (STBInfoManager.getInstance().getMapId() == -1)
            STBInfoManager.getInstance().getSectionManager().start();
    }
}

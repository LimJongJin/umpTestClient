package com.castis;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

/**
 * Created by forest on 2017-10-24.
 */
public class VODClient implements Xlet {

    public void initXlet(XletContext xc) throws XletStateChangeException {
        UMPDirector umpDirector = new UMPDirector(xc);
    }

    public void startXlet() throws XletStateChangeException {

    }

    public void pauseXlet() {

    }

    public void destroyXlet(boolean b) throws XletStateChangeException {

    }
}

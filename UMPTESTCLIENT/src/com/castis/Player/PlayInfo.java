package com.castis.Player;

public class PlayInfo {

    private String applicationName = "UMP";

    private int duration = 0;

    private long fileSize = 0L;

    private long mediaOffset = 0L;

    private double mediaRatio = 0.0;

    private byte[] ECMGroup = null;

    private String assetIdForAD = null;
    private String categoryIdForAD = null;
    
    public void init() {
	setApplicationName("UMP");
	setDuration(0);
	setFileSize(0);
	setMediaOffset(0);
	setMediaRatio(0);
	setECMGroup(null);
	setAssetIdForAD(null);
	setCategoryIdForAD(null);
	
    }

    public int getDuration() {
	return duration;
    }

    public long getFileSize() {
	return fileSize;
    }

    public long getMediaOffset() {
	return mediaOffset;
    }

    public double getMediaRatio() {
	return mediaRatio;
    }

    public void setMediaOffset(long offset) {
	this.mediaOffset = offset;
    }

    public void setMediaRatio(double ratio) {
	this.mediaRatio = ratio;
    }

    public void setFileSize(long fileSize) {
	this.fileSize = fileSize;
    }

    public void setDuration(int duration) {
	this.duration = duration;
    }

    public void setApplicationName(String applicationName) {
	this.applicationName = applicationName;
    }

    public String getApplicationName() {
	return applicationName;
    }

    public byte[] getECMGroup() {
	return ECMGroup;
    }

    public void setECMGroup(byte[] ecmgroup) {
	ECMGroup = ecmgroup;
    }

    public void setAssetIdForAD(String assetIdForAD) {
	this.assetIdForAD = assetIdForAD;
    }

    public String getAssetIdForAD() {
	return assetIdForAD;
    }

    public void setCategoryIdForAD(String categoryIdForAD) {
	this.categoryIdForAD = categoryIdForAD;
    }

    public String getCategoryIdForAD() {
	return categoryIdForAD;
    }
}

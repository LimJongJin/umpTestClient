package com.castis.Player;

import com.castis.manager.STBInfoManager;
import com.castis.pws.PWSClientController;
import com.castis.pws.PWSResult;
import com.castis.pws.protocol.PWS;
import com.castis.util.Logger;
import com.castis.vod.control.VODEventListener;
import com.castis.vod.control.VODPlayer;

import javax.media.*;
import java.io.IOException;
import java.rmi.RemoteException;

/**
 * Created by forest on 2017-10-24.
 */
public class VODPlayManager implements ControllerListener {

    private Player player = null;
    private PlayCode playCode = null;
    private PlayInfo playInfo = null;
    private PlayStatus playStatus = null;
    private boolean failed = false;
    private boolean started = false;
    private boolean realized = false;
    private boolean prefetched = false;
    private int playerStatus = VODPlayer.UMP_STATUS_INIT;

    public VODPlayManager() {
        playCode = new PlayCode();
        playInfo = new PlayInfo();
        playStatus = new PlayStatus();
    }

    public String getApplicationName() {
        return playInfo.getApplicationName();
    }

    public void setApplicationName(String name) {
        playInfo.setApplicationName(name);
    }

    public byte[] getECMGroup() {
        return playInfo.getECMGroup();
    }

    public void setECMGroup(byte[] ecmgroup) {
        playInfo.setECMGroup(ecmgroup);
    }

    public int getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayer(Player player) {
        initializeStates();
        this.player = player;
        this.player.addControllerListener(this);
    }

    private int communicate(int nodeGroupID, String macAddress, String stbModelNumber) {
        int resultCode;
        PWSResult result = PWSClientController.getInstance().requestSessionManagerAddressEx(String.valueOf(nodeGroupID),
                Integer.toString(STBInfoManager.getInstance().getMapId()));

        resultCode = getPWSResultCode(result);
        Logger.println(this, "communicate - resultCode :: " + resultCode);

        return resultCode;
    }

    private int getPWSResultCode(PWSResult pwsResult) {
        int code;
        if (isUseExtendedResultCode()) {
            code = playCode.convertToPWSResultCodeExtended(pwsResult.getResultCode());
        } else {
            code = playCode.convertToPWSResultCode(pwsResult.getResultCode());
        }
        Logger.println(this, "getPWSResultCode - code :: " + code);
        return code;
    }

    private boolean isUseExtendedResultCode = false;

    private void initializeStates() {
        realized = false;
        prefetched = false;
        started = false;
        failed = false;
    }

    boolean realize(int timeOutMillis) {
        long startTime = System.currentTimeMillis();

        synchronized (this) {
            player.realize();

            while (!realized && !failed) {
                try {
                    Thread.sleep(10);
                    Logger.println(this, "realize - realized1 :: " + realized);
                    Logger.println(this, "realize - failed1 :: " + failed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (System.currentTimeMillis() - startTime > timeOutMillis)
                    break;
            }
            Logger.println(this, "realize - realized2 :: " + realized);
            Logger.println(this, "realize - failed2 :: " + failed);
            return realized;
        }
    }

    boolean prefetch(int timeOutMillis) {
        long startTime = System.currentTimeMillis();

        synchronized (this) {
            player.prefetch();

            while (!prefetched && !failed) {
                try {
                    Thread.sleep(10);
                    Logger.println(this, "prefetch - prefetch1 :: " + prefetched);
                    Logger.println(this, "prefetch - failed1 :: " + failed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (System.currentTimeMillis() - startTime > timeOutMillis)
                    break;
            }
            Logger.println(this, "prefetch - prefetch2 :: " + prefetched);
            Logger.println(this, "prefetch - failed2 :: " + failed);
            return prefetched;
        }
    }

    public void play(int timeOutMillis) {
        Logger.println(this, "play - isReady() :: " + (isReady() ? "true" : "false"));
        if (isReady()) {
            long startTime = System.currentTimeMillis();

            synchronized (this) {
                player.start();

                while (!started && !failed) {
                    try {
                        Thread.sleep(10);
                        Logger.println(this, "play - started1 :: " + started);
                        Logger.println(this, "play - failed1 :: " + failed);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (System.currentTimeMillis() - startTime > timeOutMillis)
                        break;
                }
                Logger.println(this, "play - started2 :: " + started);
                Logger.println(this, "play - failed2 :: " + failed);
                started = true;
                failed = false;
            }
        } else {
            Logger.println(this, "Stop");
            stop();
        }
    }

    public int initForPlay(VODInfo info) {
        int result = VODPlayer.VODRESULT_SUCCESS;
        Logger.println(this, "initForPlay - playerStatus :: " + playerStatus);
        if (playerStatus == VODPlayer.UMP_STATUS_INIT) {
            playerStatus = VODPlayer.UMP_STATUS_READY;
            Logger.println(this, "initForPlay - playerStatus :: " + playerStatus);
            if (STBInfoManager.getInstance().getMapId() == -1) {
                STBInfoManager.getInstance().getSectionManager().start();
            }
            // Test할 때는 주석처리
            String pwsIp = STBInfoManager.getInstance().getPWSIpByMapId(Integer.toString(STBInfoManager.getInstance().getMapId()));
            Logger.println(this, "initForPlay - pwsIp :: " + pwsIp);
            STBInfoManager.getInstance().setPwsIp(pwsIp);
            int pwsCode = communicate(info.getNodeGroupID(), info.getMacAddress(), info.getSTBModelNumber());
            if (pwsCode == PWS.SERVER_SUCCESS) {
                try {
                    player = Manager.createPlayer(new MediaLocator("cirtsp://10.10.70.102/M0133710.mpg"));
                    setPlayer(player);
                    if (realize(3000)) {
                        Logger.println(this, "realize ~");
                        if (prefetch(3000)) {
                            Logger.println(this, "prefetch ~");
                            play(3000);
                            Logger.println(this, "play ~");
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NoPlayerException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (result == VODPlayer.VODRESULT_SUCCESS) {
                    playerStatus = VODPlayer.UMP_STATUS_PLAYING;
                } else {
                    playerStatus = VODPlayer.UMP_STATUS_INIT;
                }
            } else {
                result = pwsCode;
                playerStatus = VODPlayer.UMP_STATUS_INIT;
            }
        } else {
            result = VODPlayer.VODRESULT_SERVER_ERROR;
        }
        return result;
    }

    public void stop() {
        if (playerStatus != VODPlayer.UMP_STATUS_READY) {
            started = false;
            if (player != null) {
                player.removeControllerListener(this);
                this.player.close();
                player = null;
            }
            playerStatus = VODPlayer.UMP_STATUS_INIT;
        }
    }

    private boolean isReady() {
        return (player != null && player.getState() == Player.Prefetched);
    }

    public void setUseExtendedResultCode(boolean isUseExtendedResultCode) {
        this.isUseExtendedResultCode = isUseExtendedResultCode;
    }

    public boolean isUseExtendedResultCode() {
        return isUseExtendedResultCode;
    }

    public void setAssetIdForAD(String assetIdForAD) {
        playInfo.setAssetIdForAD(assetIdForAD);
    }

    public void setCategoryIdForAD(String categoryIdForAD) {
        playInfo.setCategoryIdForAD(categoryIdForAD);
    }

    public void controllerUpdate(ControllerEvent controllerEvent) {
        if (controllerEvent instanceof RealizeCompleteEvent) {
            Logger.println(this, "realized is TRUE");
            realized = true;
        } else if (controllerEvent instanceof PrefetchCompleteEvent) {
            Logger.println(this, "prefetched is TRUE");
            prefetched = true;
        } else if (controllerEvent instanceof ControllerErrorEvent || controllerEvent instanceof ConnectionErrorEvent) {
            Logger.println(this, "failed is TRUE");
            Logger.println(this, controllerEvent.toString());
            failed = true;
        } else if (controllerEvent instanceof StartEvent) {
            Logger.println(this, "started is TRUE");
            started = true;
        } else if (controllerEvent instanceof EndOfMediaEvent) {

        } else if (controllerEvent instanceof DeallocateEvent) {

        } else if(controllerEvent instanceof StopEvent) {

        }
    }
}
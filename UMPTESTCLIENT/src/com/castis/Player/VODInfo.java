package com.castis.Player;

public class VODInfo {

    private String srmIP = null;

    private String fileName = null;
    
    private int[] nodeGroup = null;
    
    private int nodeGroupID = 0;

    private int SOID = 0;

    private String smartcardID = null;

    private String pinCode = null;

    private int dsmccAdaptationType = 0;

    private byte[] encryptionData = null;

    private String[] menuID = null;

    private long offset = 0;

    private int requestID = 0;

    private String vodRequestID = null;
    
    private String macAddress;
    
    private String stbModelNumber;
    
    public VODInfo() {
    }

    public VODInfo(String fileName, int nodeGroupID, long offset, int requestID , String macAddress , String stbModelNumber) {
	setFileName(fileName);
	setNodeGroupID(nodeGroupID);
	setOffset(offset);
	setRequestID(requestID);
	this.macAddress = macAddress;
	this.stbModelNumber = stbModelNumber;
    }
    
    public String getMacAddress(){
	return this.macAddress;
    }
    
    public void setMacAddress(String macAddress){
	this.macAddress = macAddress;
    }
    
    public String getSTBModelNumber(){
	return this.stbModelNumber;
    }
    
    public void setSrmIP(String srmIP) {
	this.srmIP = srmIP;
    }

    public String getSrmIP() {
	return srmIP;
    }

    public void setFileName(String fileName) {
	this.fileName = fileName;
    }

    public String getFileName() {
	return fileName;
    }

    public void setNodeGroup(int[] nodeGroup) {
	this.nodeGroup = nodeGroup;
    }

    public int[] getNodeGroup() {
	return nodeGroup;
    }
    
    public void setSOID(int sOID) {
	SOID = sOID;
    }

    public int getSOID() {
	return SOID;
    }

    public void setSmartcardID(String smartcardID) {
	this.smartcardID = smartcardID;
    }

    public String getSmartcardID() {
	return smartcardID;
    }

    public void setPinCode(String pinCode) {
	this.pinCode = pinCode;
    }

    public String getPinCode() {
	return pinCode;
    }

    public void setDsmccAdaptationType(int dsmccAdaptationType) {
	this.dsmccAdaptationType = dsmccAdaptationType;
    }

    public int getDsmccAdaptationType() {
	return dsmccAdaptationType;
    }

    public void setEncryptionData(byte[] encryptionData) {
	this.encryptionData = encryptionData;
    }

    public byte[] getEncryptionData() {
	return encryptionData;
    }

    public void setMenuID(String[] menuID) {
	this.menuID = menuID;
    }

    public String[] getMenuID() {
	return menuID;
    }

    public void setOffset(long offset) {
	this.offset = offset;
    }

    public long getOffset() {
	return offset;
    }

    public void setNodeGroupID(int nodeGroupID) {
	this.nodeGroupID = nodeGroupID;
    }

    public int getNodeGroupID() {
	return nodeGroupID;
    }
    
    public void setRequestID(int requestID) {
	this.requestID = requestID;
    }

    public int getRequestID() {
	return requestID;
    }

    public void setVodRequestID(String vodRequestID) {
	this.vodRequestID = vodRequestID;
    }

    public String getVodRequestID() {
	return vodRequestID;
    }

    
}

package com.castis.Player;

public class PlayStatus {

    public static final int PLAY = 1;

    public static final int PAUSE = 0;

    public static final int REWIND_2X = -2;

    public static final int REWIND_4X = -4;

    public static final int REWIND_8X = -8;

    public static final int REWIND_16X = -16;

    public static final int REWIND_32X = -32;

    public static final int REWIND_64X = -64;

    public static final int FASTFORWARD_2X = 2;

    public static final int FASTFORWARD_4X = 4;

    public static final int FASTFORWARD_8X = 8;

    public static final int FASTFORWARD_16X = 16;

    public static final int FASTFORWARD_32X = 32;

    public static final int FASTFORWARD_64X = 64;

    private int status = -1;
    
    public boolean isValidRate(int rate) {
	switch (rate) {
	case PLAY:
	case PAUSE:
	case REWIND_2X:
	case REWIND_4X:
	case REWIND_8X:
	case REWIND_16X:
	case REWIND_32X:
	case REWIND_64X:
	case FASTFORWARD_2X:
	case FASTFORWARD_4X:
	case FASTFORWARD_8X:
	case FASTFORWARD_16X:
	case FASTFORWARD_32X:
	case FASTFORWARD_64X:
	    return true;
	}
	return false;
    }

    public boolean isValidStatus(int rate) {
	if (getStatus() == rate)
	    return false;
	else
	    return true;
    }

    public void setStatus(int status) {
	this.status = status;
    }

    public int getStatus() {
	return status;
    }

    public boolean isPauseStatus() {

	return (getStatus() == PAUSE) ? true : false;
    }

    public boolean isTrickStatus() {
	return ((getStatus() > PLAY) || (getStatus() < PAUSE)) ? true : false;
    }

    public void init() {
	setStatus(-1);
    }
}

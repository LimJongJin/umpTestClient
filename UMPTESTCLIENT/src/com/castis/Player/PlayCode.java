package com.castis.Player;

import com.castis.pws.protocol.PWS;
import com.castis.util.Logger;
import com.castis.vod.VodClientResult_enum;
import com.castis.vod.control.VODPlayer;

public class PlayCode {

	public int convertToPlayCode(int VODCode) {
		Logger.println(this, "convertToPlayCode ( " + VODCode + ")");
		switch (VODCode) {
		case VodClientResult_enum.VODCLIENT_SUCCESS:
			return VODPlayer.VODRESULT_SUCCESS;
		case VodClientResult_enum.VODCLIENT_ERROR_DSMCC_INIT_IO_ERROR:
			return VODPlayer.VODRESULT_INVALID_FILE;
		case VodClientResult_enum.VODCLIENT_ERROR_INVALID_TSID:
			return VODPlayer.VODRESULT_INVALID_TSID;
		case VodClientResult_enum.VODCLIENT_ERROR_SRM_MAXSESSION_ERROR:
		case VodClientResult_enum.VODCLIENT_ERROR_NOT_ENOUGH_NETWORK_RESOURCE:
		case VodClientResult_enum.VODCLIENT_ERROR_MAX_BANDWIDTH_ERROR:
		case VodClientResult_enum.VODCLIENT_ERROR_MAXSESSION_ERROR:
		case VodClientResult_enum.VODCLIENT_ERROR_NOT_ENOUGH_BANDWIDTH:
			return VODPlayer.VODRESULT_MAX_SESSION_ERROR;
		default:
			return VODPlayer.VODRESULT_SERVER_ERROR;
		}
	}

	public int convertToPWSResultCode(int code) {
		switch (code) {
		case PWS.SERVER_SUCCESS:
			break;
		case PWS.INVALID_USER:
			code = VODPlayer.VODRESULT_INVALID_USER;
			break;

		default:
			code = VODPlayer.VODRESULT_SERVER_ERROR;
			break;
		}

		return code;
	}

	public int convertToPWSResultCodeExtended(int code) {
		switch (code) {
		case PWS.SERVER_SUCCESS:
			break;
		case PWS.INVALID_USER:
			code = VODPlayer.VODRESULT_INVALID_USER;
			break;
		case PWS.SOCKETFAIL:
			code = VodClientResult_enum.UMP_ERROR_SOCKET_ERROR;
			break;

		case PWS.RECEIVE_FAIL:
			code = VodClientResult_enum.UMP_ERROR_RECEIVE_SESSIONADDRESS;
			break;

		default:
			code = VODPlayer.VODRESULT_SERVER_ERROR;
			break;
		}

		return code;
	}

	public int convertToPlayCodeExtended(int VODCode) {
		Logger.println(this, "convertToPlayCode ( " + VODCode + ")");
		switch (VODCode) {
		case VodClientResult_enum.VODCLIENT_SUCCESS:
			return VODPlayer.VODRESULT_SUCCESS;

		default:
			return VODCode;
		}
	}

}

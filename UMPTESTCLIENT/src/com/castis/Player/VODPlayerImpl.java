package com.castis.Player;

import com.castis.manager.STBInfoManager;
import com.castis.util.Logger;
import com.castis.vod.control.VODEventListener;
import com.castis.vod.control.VODPlayer;

import java.rmi.RemoteException;

/**
 * Created by forest on 2017-10-25.
 */
public class VODPlayerImpl implements VODPlayer {

    private VODPlayManager vodPlayManager = null;

    public VODPlayerImpl(VODPlayManager vodPlayManager) {
        this.vodPlayManager = vodPlayManager;
    }

    public int play(String fileName, int[] TSList, long offset, int requestID, int priorityCriteria) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - play1");
        return VODRESULT_INVALID_PARAMETER;
    }

    public int play(String fileName, int[] TSList, long offset, int requestID) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - play2");
        return VODRESULT_INVALID_PARAMETER;
    }

    public int play(String fileName, int nodeGroupID, long offset, int requestID) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - play3");
        int result;
        if (fileName == null || fileName.equals("") || nodeGroupID <= 0 || offset < 0 || requestID < 0) {
            result = VODPlayer.VODRESULT_INVALID_PARAMETER;
        } else {
            result = vodPlayManager.initForPlay(new VODInfo(fileName, nodeGroupID, offset, requestID, STBInfoManager.getInstance().getMacAddress(), STBInfoManager.getInstance().getSTBModelNumber()));
        }
        Logger.println(this, "VODPlayerImpl - play - result :: " + result);

        return result;
    }

    public int play(String fileName, int nodegroupID, long offset, int requestID, int priorityCriteria) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - play4");
        return VODRESULT_INVALID_PARAMETER;
    }

    public void setApplicationName(String name) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - setApplicationName");
        Logger.println(this, "setApplicationName = " + name);
        vodPlayManager.setApplicationName(name);
    }

    public void setECMGroup(byte[] ecmgroup) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - setECMGroup");
        vodPlayManager.setECMGroup(ecmgroup);
    }

    public void changeVODChannel(int x, int y, int width, int height) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - changeVODChannel");
    }

    public void stop() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - stop");
        vodPlayManager.stop();
    }

    public int getDuration() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getDuration");
        return 0;
    }

    public long getFileSize() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getFileSize");
        return 1000000;
    }

    public long getMediaOffset() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getMediaOffset");
        return 1;
    }

    public double getMediaRatio() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getMediaRatio");
        return 0;
    }

    public boolean setMediaOffset(long offset) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - setMediaOffset");
        return true;
    }

    public boolean setMediaRatio(double ratio) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - setMediaRatio");
        return true;
    }

    public boolean setRate(int rate) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - setRate");
        return true;
    }

    public void addVODEventListener(VODEventListener eventListener) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - addVODEventListener");
    }

    public void removeVODEventListener(VODEventListener eventListener) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - removeVODEventListener");
    }

    public int getFrequency() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getFrequency");
        return 0;
    }

    public int getProgramNumber() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getProgramNumber");
        return 0;
    }

    public void demandVODRequestID(String assetID, String categoryID) throws RemoteException {
        Logger.println(this, "VODPlayerImpl - demandVODRequestID");
    }

    public void useExtendedResultCode() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - useExtendedResultCode");
        vodPlayManager.setUseExtendedResultCode(true);
    }

    public boolean isPlayingAdvertisement() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - isPlayingAdvertisement");
        return false;
    }

    public int getUMPStatus() throws RemoteException {
        Logger.println(this, "VODPlayerImpl - getUMPStatus");
        return vodPlayManager.getPlayerStatus();
    }

    public String getUMPVersion() throws RemoteException {
        Logger.println(this, "VODPlayerIVODPlayerImplmplForIPVOD - getUMPVersion");
        return STBInfoManager.VERSION + "(" + STBInfoManager.getInstance().getPwsIp() + ")";
    }

}

package com.castis.pws;

import java.util.Hashtable;

public class PWSParametersTable {

	public PWSParametersTable() {

	}

	Hashtable createPWSDefaultParamTable() {
		Hashtable hashtable = new Hashtable();

		return hashtable;
	}

	public Hashtable getRequestAuthTable(String terminalId, String umpVersion, String stbModelNumber) {
		Hashtable hashtable = createPWSDefaultParamTable();
		hashtable.put("terminalId", terminalId);
		hashtable.put("authProfile", "1");
		hashtable.put("clientVersion", umpVersion);
		hashtable.put("hardwareModel", stbModelNumber);

		return hashtable;
	}

	public Hashtable getRequestEncryptionTable(String terminalKey, String assetID, String productID, String goodID,
			String entitlementID, String superCasID, String smartCardID) {
		Hashtable hashtable = createPWSDefaultParamTable();
		hashtable.put("terminalKey", terminalKey);
		hashtable.put("assetId", assetID);
		hashtable.put("productId", productID);
		hashtable.put("goodId", goodID);
		hashtable.put("entitlementId", entitlementID);
		hashtable.put("superCasId", superCasID);
		hashtable.put("smartCardId", smartCardID);

		return hashtable;
	}

	public Hashtable getVODRequestID(String userId, String assetId, String categoryId, String soId,
			String offset) {
		Hashtable hashtable = createPWSDefaultParamTable();
		hashtable.put("userId", userId);
		hashtable.put("assetId", assetId);
		hashtable.put("categoryId", categoryId);
		hashtable.put("soId", soId);
		hashtable.put("offset", offset);

		return hashtable;
	}

	public Hashtable getSessionManagerAddressEx(String userId, String nodeGroupId, String mapId) {
		Hashtable hashtable = createPWSDefaultParamTable();
		hashtable.put("userId", userId);
		hashtable.put("networkCell", mapId + "/" + nodeGroupId);

		return hashtable;
	}

}

package com.castis.pws.protocol;

public class PWS {

	public static final String DATATYPE_JSON = "json";

	public static final String DATATYPE_XML = "xml";

	public static final int JSON = 0;

	public static final int XML = 1;

	public static final String GET = "GET";

	public static final String POST = "POST";

	public static final String TRAVERSETYPE_DFS = "DFS";

	public static final String TRAVERSETYPE_BFS = "BFS";

	public static final int SUCCESS = 0;

	public static final int SOCKETFAIL = 1;

	public static final int FAIL = 2;

	public static final int RECEIVE_FAIL = 3;

	public static final int INVALIDPARAMETER = 4;

	public static final int INVALID_USER = 5;

	public static final int SERVER_SUCCESS = 200;

	public static final int SERVER_SUCCESS_REQUESTID = 201;

	/** PWSPROTOCOL NAME **/

	public static final String VODREQUESTID = "vodRequestIds";

	public static final String SESSION_MANAGER_ADDRESSEX = "sessionAddresses";

}

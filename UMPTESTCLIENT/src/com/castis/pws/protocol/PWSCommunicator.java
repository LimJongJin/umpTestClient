package com.castis.pws.protocol;

import com.castis.pws.PWSException;
import com.castis.pws.PWSResult;
import com.castis.util.Logger;
import org.json.me.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class PWSCommunicator {

	protected String PWS_NAME = "pws";

	protected String PWS_IP = null;

	protected String PWS_PORT = null;

	protected String PWS_PROTOCOL_NAME = null;

	protected String PWS_URL = null;

	protected String REQUEST_METHOD_TYPE = PWS.GET;

	protected int checkPeriodMilleSec = 100;

	protected int readTimeOutMilleSec = 5000;

	protected HttpURLConnection connection = null;

	public PWSCommunicator(String pwsIp, String pwsPort) {
		this.PWS_IP = pwsIp;
		this.PWS_PORT = pwsPort;
	}
	
	public void sessionManagerAddressex(String protocolName, String userId, String mapId, String nodeGroupId) throws PWSException {
		this.PWS_PROTOCOL_NAME = protocolName;
		try {
			this.PWS_URL = getSessionAdressGetURL(userId, mapId, nodeGroupId);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public void getVodRequestIds(String protocolName) throws PWSException {
		this.PWS_PROTOCOL_NAME = protocolName;
		try {
			this.PWS_URL = getRequestPostURL();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void setTimeout(int timeOutMilleSec) {
		this.readTimeOutMilleSec = timeOutMilleSec;
	}

	protected HttpURLConnection getConnection() {
		Connect connect = new Connect(PWS_URL);
		connect.setTimeout(readTimeOutMilleSec);

		if (connect.connect()) {
			return connect.getConnection();
		} else
			return null;
	}

	public Object getSessionManagerAddressEx(boolean readAll) throws PWSException {
		Logger.println(this, " START / PWS URL : " + PWS_URL);
		PWSResult returnData = new PWSResult();

		BufferedReader ireader = null;
		try {
			URL url = new URL(PWS_URL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", "token umptokentest1234");
			ireader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

			if (readAll) {
				String str;
				ArrayList arr = new ArrayList();
				while ((str = ireader.readLine()) != null) {
					arr.add(str);
				}
				returnData.setData(arr);
				str = null;
			} else {
				String str;
				while ((str = ireader.readLine()) != null) {
					returnData.setData(str);
				}
				str = null;
			}
			returnData.setResultCode(PWS.SUCCESS);
			Logger.println(this, "returnData.setData");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			throw new PWSException(PWS.SOCKETFAIL);
		} catch (IOException e) {
			e.printStackTrace();
			throw new PWSException(PWS.SOCKETFAIL);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PWSException(PWS.SOCKETFAIL);
		} finally {
			if (connection != null)
				connection.disconnect();
			if (ireader != null)
				try {
					ireader.close();
					ireader = null;
				} catch (IOException e) {
					e.printStackTrace();
					new PWSException(PWS.FAIL);
				}
			Logger.println(this, "END");
		}
		return returnData;
	}
	
	public Object getVodRequestId(String userId, String assetID, String categoryID, String soID, String offset) throws PWSException {
		Logger.println(this, " START / PWS URL : " + PWS_URL);
		PWSResult returnData = new PWSResult();

		BufferedReader ireader = null;

		try {

			URL url = new URL(PWS_URL);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Authorization", "token umptokentest1234");
			
	        JSONObject json=new JSONObject();
	        json.put("userId", userId);
	        json.put("assetId", assetID);
	        json.put("categoryId", categoryID);
	        json.put("soId", soID);
	        json.put("offset", offset);

			connection.setDoOutput(true);
	        OutputStreamWriter output=new OutputStreamWriter(connection.getOutputStream());
	        Logger.println(this,  "json :: " + json);
	        output.write(json.toString());
	        output.flush();
	        
			ireader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

			String str;
			while ((str = ireader.readLine()) != null) {
				returnData.setData(str);
			}
			str = null;

			String vodRequestID=connection.getResponseMessage();
	        Logger.println(this, "vodRequestID :: " + vodRequestID);

			returnData.setResultCode(PWS.SUCCESS);
			Logger.println(this, "returnData.setData");
	    } catch (MalformedURLException e) {
			e.printStackTrace();
			throw new PWSException(PWS.SOCKETFAIL);
		} catch (IOException e) {
			e.printStackTrace();
			throw new PWSException(PWS.SOCKETFAIL);
		} catch (Exception e) {
			e.printStackTrace();
			throw new PWSException(PWS.SOCKETFAIL);
		} finally {
			if (connection != null)
				connection.disconnect();
			Logger.println(this, "END");
		}
	    return returnData;
	}
	
	private String getSessionAdressGetURL(String userId, String mapId, String nodeGroupId) throws PWSException, UnsupportedEncodingException {
		return createURL() + buildParameter(userId, mapId, nodeGroupId);
	}

	private String getRequestPostURL() throws PWSException, UnsupportedEncodingException {
		return createURL();
	}

	private String createURL() throws PWSException {
		Logger.println(this, "PWS_IP :: " + this.PWS_IP + ", PWS_PORT :: " + this.PWS_PORT + ", PWS_PROTOCOL_NAME :: " + this.PWS_PROTOCOL_NAME);
		if (this.PWS_IP == null || this.PWS_PORT == null || this.PWS_PROTOCOL_NAME == null) {
			throw new PWSException(PWS.INVALIDPARAMETER);
		}
		String url = "http://" + this.PWS_IP + ":" + this.PWS_PORT + "/" + this.PWS_NAME + "/" + this.PWS_PROTOCOL_NAME + "/";
		return url;
	}

	private String buildParameter(String userId, String mapId, String nodeGroupId) throws PWSException, UnsupportedEncodingException {
		if (userId == null || mapId == null || nodeGroupId == null || userId == "" || mapId == "" || nodeGroupId == "")
			throw new PWSException(PWS.INVALIDPARAMETER);

		StringBuffer sb = new StringBuffer();

		sb.append("userID/");
		sb.append(userId + "/");
		sb.append("networkCell/");
		sb.append(mapId + "/");
		sb.append(nodeGroupId + "/");
		return sb.toString();
	}

}

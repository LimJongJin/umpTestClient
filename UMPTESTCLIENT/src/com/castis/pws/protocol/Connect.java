package com.castis.pws.protocol;

import com.castis.pws.http.HttpTimeoutHandler;
import com.castis.util.Logger;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class Connect implements Runnable {

	Thread connectThread = null;

	boolean isConnect = false;

	int checkCount = 30;

	String URLStr;

	int timeout = 3000;

	HttpURLConnection connection = null;

	public Connect(String URLString) {
		this.URLStr = URLString;
	}

	public HttpURLConnection getConnection() {
		return connection;
	}

	public void run() {
		isConnect = false;
		URL url;
		try {
			url = new URL((URL) null, URLStr, new HttpTimeoutHandler(timeout));
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();
			isConnect = true;
			Logger.println(this, "Connection Success");
		} catch (MalformedURLException e) {
			e.printStackTrace();
			connection = null;
		} catch (ProtocolException e) {
			e.printStackTrace();
			connection = null;
		} catch (IOException e) {
			e.printStackTrace();
			connection = null;
		}
	}

	public boolean connect() {
		Logger.println(this, "Connect:try to connect PWS Server");
		connectThread = new Thread(this, "PWS");
		connectThread.start();
		for (int i = 0; i < checkCount; i++) {
			Logger.println(this, "HttpConnection Check Number : " + i);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (isConnect)
				break;
			if (i >= checkCount - 1 && connectThread.isAlive()) {
				connectThread.interrupt();
				connectThread = null;
				connection = null;
				Logger.println(Logger.ERROR, this, "Connect Fail:could not connect PWS Server");
				return false;
			}
		}
		Logger.println(this, "Connect Success:connect PWS Server");
		return true;
	}

	public void close() {
		try {
			if (connection != null) {
				connection.disconnect();
				Logger.println(this, "PWS DisConnection!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection = null;
		}
	}

	public void setTimeout(int readTimeOutMilleSec) {
		this.timeout = readTimeOutMilleSec;
	}

}

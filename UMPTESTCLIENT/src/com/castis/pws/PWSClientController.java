package com.castis.pws;

import com.castis.manager.STBInfoManager;
import com.castis.pws.protocol.PWS;
import org.json.me.JSONException;
import org.json.me.JSONObject;

public class PWSClientController {

	private static final PWSClientController instance = new PWSClientController();
	private PWSClient client = null;

	public static PWSClientController getInstance() {
		return instance;
	}

	private PWSClientController() {
		client = new PWSClient();
	}

	public PWSResult requestSessionManagerAddressEx(String nodeGroupId, String mapId) {
		PWSResult result = null;
		try {
			result = client.getSessionManagerAddressEx(STBInfoManager.getInstance().getMacAddress(), nodeGroupId, mapId);
			if (result.getResultCode() == PWS.SUCCESS) {
				try {
					JSONObject data = new JSONObject((String) result.getData());
					result.setResultCode(PWS.SERVER_SUCCESS);
					if(data.getString("srmAddress") != null) {
						STBInfoManager.getInstance().setSRMIP(data.getString("srmAddress"));
					} else {
						result.setResultCode(PWS.RECEIVE_FAIL);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					result.setResultCode(PWS.FAIL);
				}
			}
		} catch (PWSException e) {
			e.printStackTrace();
			result = new PWSResult();
			result.setResultCode(e.getResultCode());
		}
		return result;
	}

	public PWSResult requestVODRequestID(String assetID, String categoryID, String soID, String offset) {
		PWSResult result = null;
		try {
			result = client.getVODRequestID(STBInfoManager.getInstance().getMacAddress(), assetID, categoryID, soID, offset);
			if (result.getResultCode() == PWS.SUCCESS) {
				try {
					JSONObject data = new JSONObject((String) result.getData());
					result.setResultCode(PWS.SERVER_SUCCESS);
					if (data.getString("vodRequestId") != null) {
						result.setData(data.getString("vodRequestId"));
					} else {
						result.setResultCode(PWS.RECEIVE_FAIL);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					result.setResultCode(PWS.FAIL);
				}
			}
		} catch (PWSException e) {
			e.printStackTrace();
			result = new PWSResult();
			result.setResultCode(e.getResultCode());
		}
		return result;
	}
}

package com.castis.pws;

import com.castis.manager.STBInfoManager;
import com.castis.pws.protocol.PWS;
import com.castis.pws.protocol.PWSCommunicator;

public class PWSClient {
	private String pwsIp = "10.9.50.7";

	private String pwsPort = "8085";

	public PWSClient() {}

	PWSCommunicator getPWSCommunicator() {
		pwsIp = STBInfoManager.getInstance().getPwsIp();
		pwsPort = STBInfoManager.getInstance().getPwsPort();
		return new PWSCommunicator(pwsIp, pwsPort);
	}

	public PWSResult getSessionManagerAddressEx(String userId, String nodeGroupId, String mapId) throws PWSException {
		PWSCommunicator pwsCommunicator = getPWSCommunicator();
		pwsCommunicator.sessionManagerAddressex(PWS.SESSION_MANAGER_ADDRESSEX, userId, mapId, nodeGroupId);
		pwsCommunicator.setTimeout(10000);
		return (PWSResult) pwsCommunicator.getSessionManagerAddressEx(false);
	}

	public PWSResult getVODRequestID(String userId, String assetID, String categoryID, String soID, String offset)
			throws PWSException {
		PWSCommunicator pwsCommunicator = getPWSCommunicator();
		pwsCommunicator.getVodRequestIds(PWS.VODREQUESTID);
		pwsCommunicator.setTimeout(10000);
		return (PWSResult) pwsCommunicator.getVodRequestId(userId, assetID, categoryID, soID, offset);

	}
}

package com.castis.pws;

public class PWSResult {

	private int result;
	private Object resultData;
	private int totalCount;
	private int totalPage;

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public void setResultCode(int result) {
		this.result = result;
	}

	public int getResultCode() {
		return result;
	}

	public void setData(Object resultData) {
		this.resultData = resultData;
	}

	public Object getData() {
		return resultData;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

}

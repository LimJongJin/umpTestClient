package com.castis.pws.http;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;

public class HttpTimeoutFactory implements URLStreamHandlerFactory {
	private int iSoTimeout = 0;

	public HttpTimeoutFactory(int iSoTimeout) {
		this.iSoTimeout = iSoTimeout;
	}

	public URLStreamHandler createURLStreamHandler(String str) {
		return new HttpTimeoutHandler(iSoTimeout);
	}

}

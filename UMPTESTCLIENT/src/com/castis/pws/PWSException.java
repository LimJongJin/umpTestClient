package com.castis.pws;

public class PWSException extends Exception {

	int resultCode;

	public PWSException() {

	}

	public PWSException(int resultCode) {
		this.resultCode = resultCode;
	}

	public int getResultCode() {
		return resultCode;
	}

}

package com.castis.ixc;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by forest on 2017-10-24.
 */
public interface VODServiceManager extends Remote {

        /** RMI APP NAME */
        public static final String RMI_APP_NAME = "VODServiceManager";

        /**
         * 최신영화관 메뉴 표시 요청
         *
         * @throws RemoteException
         */
        public void showNewVODMenu() throws RemoteException;

        /**
         * TV 다시보기 메뉴 표시 요청
         *
         * @throws RemoteException
         */
        public void showVODMenu() throws RemoteException;

        /**
         * 우리동내 메뉴 진입 요청
         * categoryID, assetID 가 있으면 우리동내내의 해당 컨텐츠, 카테고리로 이동
         * null 일경우 기본 우리동내 메뉴 (vodmenu 형태)
         * @throws RemoteException
         */
        public void showMyTownMenu(String categoryID, String assetID) throws RemoteException;

        /**
         * 서비스안내 표시 요청
         *
         * @throws RemoteException
         */
        public void showHelpMenu() throws RemoteException;

        /**
         * @deprecated
         * @throws RemoteException
         */
        public void showMyPageMenu() throws RemoteException;

        /**
         * 마이컨텐츠 표시 요청
         *
         * @throws RemoteException
         */
        public void showMyContentMenu() throws RemoteException;

        /**
         * VOD Channel 에서 컨텐츠 상세정보 표시 요청
         *
         * @param assetID
         *            요청하는 컨텐츠 ID
         * @param isParentalControlChecked
         *            시청연령제한 인증 여부
         * @throws RemoteException
         */
        public void showVODChannelContent(String assetID, boolean isParentalControlChecked) throws RemoteException;

        /**
         * VOD Channel 에서 카테고리 표시 요청
         *
         * @param categoryID
         *            요청하는 카테고리 ID
         * @param isParentalControlChecked
         *            시청연령제한 인증 여부
         * @throws RemoteException
         */
        public void showVODChannelCategory(String categoryID, boolean isParentalControlChecked) throws RemoteException;

        /**
         * VOD Channel 에서 VOD 상품 가입 요청
         *
         * @param firstCategoryID
         *            상품이 연결되어 있는 카테고리 ID
         * @param secondCategoryID
         *            가입 후 이동하려는 카테고리 ID
         * @param isParentalControlChecked
         *            시청연령제한 인증 여부
         * @throws RemoteException
         */
        public void showVODChannelPackage(String firstCategoryID, String secondCategoryID, boolean isParentalControlChecked) throws RemoteException;

        /**
         * 핫 이슈 상세정보 표시 요청
         * @deprecated
         * @param bannerID
         *            요청하는 핫이슈ID
         * @throws RemoteException
         */
        public void showHotIssueContent(String bannerID) throws RemoteException;

        /**
         * VOD 검색 결과에서 선택된 컨텐츠 정보 표시 요청
         *
         * @param assetID
         *            요청하는 컨텐츠 ID
         * @param isParentalControlChecked
         *            시청연령제한 인증 여부
         * @throws RemoteException
         */
        public void showVODSearchedContent(String assetID, boolean isParentalControlChecked) throws RemoteException;

        /**
         * VOD 검색 결과에서 시리즈 컨텐츠 정보 표시 요청
         *
         * @param categoryID
         *            요청하는 컨텐츠의 menuID
         * @param isParentalControlChecked
         *            시청연령제한 인증 여부
         * @throws RemoteException
         */
        public void showVODSearchedSeriesContent(String categoryID, boolean isParentalControlChecked) throws RemoteException;

        public void showPromoContent(String assetID, int epgStateId) throws RemoteException;

        /**
         * 보유 쿠폰 목록 팝업으로 이동 요청
         *
         * @throws RemoteException
         */
        public void goToMyCouponListPopup() throws RemoteException;

        /**
         * 화면에 표시된 VOD 서비스를 닫도록 요청
         *
         * @throws RemoteException
         */
        public void hideVODService() throws RemoteException;

        /**
         * VOD 재생 중에 HomeMenu 가 표시될 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyHomeMenuShowingWhileVODWatching() throws RemoteException;

        /**
         * VOD 재생 중에 HomeMenu 가 사라질 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyHomeMenuHidingWhileVODWatching() throws RemoteException;

        /**
         * 성인인증 사용여부 변경시 알려줌
         *
         * @param isAdultCheck
         *            성인인증 사용여부 0: 허용, 1: 제한, 2: 숨김
         * @throws RemoteException
         */
        public void notifyAdultCheckChanging(int isAdultCheck) throws RemoteException;

        /**
         * VOD 간편구매여부 변경시 알려줌
         *
         * @param isUsing
         *            사용여부
         * @throws RemoteException
         */
        public void notifySimplePurchaseChanging(boolean isUsing) throws RemoteException;

        /**
         * 시리즈 연속보기 시청여부 변경시 알려줌
         *
         * @param isUsing
         *            사용여부
         * @throws RemoteException
         */
        public void notifySeriesPlayOptionChanging(boolean isUsing) throws RemoteException;

        /**
         * 외부에서 쿠폰금액이 변경될 경우 알려줌
         *
         * @throws RemoteException
         */
        public void updateCouponCash() throws RemoteException;

        /**
         * VOD 에서 쿠폰충전소 진입후 VOD 로 되돌아 올 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyBackFromCouponStation() throws RemoteException;

        /**
         * 마이 컨텐츠의 자유 이용권 진입후 되돌아 올 경우 알려줌
         * @deprecated
         * @throws RemoteException
         */
        public void notifyBackFromPackagePortal() throws RemoteException;

        /**
         * 퀵메뉴 안내 표시여부 변경시 알려줌
         *
         * @param isDisplaying
         *            표시여부
         * @throws RemoteException
         */
        public void notifyQuickMenuDisplayChanging(boolean isDisplaying) throws RemoteException;

        /**
         * VOD 재생 중 종료 팝업을 띄우도록 요청
         *
         * @throws RemoteException
         */
        public void requestVODPlayStop() throws RemoteException;

        /**
         * VOD 카테고리 이동 요청
         *
         * @param categoryID
         *            카테고리ID
         * @param epgStateId
         *            EPG State ID
         * @throws RemoteException
         */
        public void goToVODCategory(String categoryID, int epgStateId) throws RemoteException;

        /**
         * VOD 컨텐츠 이동 요청
         *
         * @param assetID
         *            Asset ID
         * @param epgStateId
         *            EPG State ID
         * @throws RemoteException
         */
        public void goToVODContent(String assetID, int epgStateId) throws RemoteException;

        /**
         * SVOD 상품 이동 요청
         *
         * @param firstCategoryID
         *            상품이 걸려있는 Category ID
         * @param secondCategoryID
         *            이동할 Category ID
         * @param epgStateId
         *            EPG State ID
         * @throws RemoteException
         */

        public void goToSVODContent(String firstCategoryID, String secondCategoryID, int epgStateId) throws RemoteException;

        /**
         * 모바일 결제 사용여부 변경시 알려줌
         *
         * @param isUsing
         *            모바일 결제 사용여부
         * @throws RemoteException
         */
        public void notifyMobilePayChanged(boolean isUsing) throws RemoteException;

        /**
         * HotEvent 상세화면 이동 요청
         *
         * @param eventID
         * @param epgStateID
         * @throws RemoteException
         */
        public void goToHotEvent(String eventID, int epgStateID) throws RemoteException;

        /**
         * SVOD 상품의 카테고리 이동 요청
         *
         * @param categoryId
         *            SVOD상품이 포함되어 있는 Category ID(2dpth까지 제한)
         * @throws RemoteException
         */
        public void goToSVODCategory(String categoryID) throws RemoteException;

        /**
         * 쿠폰BOX 이동 요청
         *
         * @param epgStateID
         * @throws RemoteException
         */
        public void goToCouponBox(int epgStateID) throws RemoteException;

        /**
         * 쿠폰BOX 리스트의 상세화면 이동 요청
         *
         * @param couponID
         * @param epgStateID
         * @throws RemoteException
         */
        public void goToCouponContent(String couponID, int epgStateID) throws RemoteException;

        /**
         * VOD 에서 검색 팝업 진입후 VOD 로 되돌아 올 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyBackFromSearchInputPopup() throws RemoteException;

        /**
         * VOD 상세 화면에서 검색 결과 진입후 VOD 상세화면으로 되돌아 올 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyBackFromSearchResultPopup() throws RemoteException;

        /**
         * 공지사항 비밀번호 변경 팝업 진입후 공지사항 팝업으로 되돌아 올 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyBackFromPinChangePopup() throws RemoteException;

        /**
         * 마이 컨텐츠의 정기 구매 진입후 되돌아 올 경우 알려줌
         *
         * @throws RemoteException
         */
        public void notifyBackFromVipCoinMenu() throws RemoteException;

        /**
         * 연관 컨텐츠 최근 시청 VOD 에서 VOD 바로 재생 요청
         * {@link #com.nds.epg.cjh.ixc.vod.VodBaseApi.}
         * @param assetID
         * @throws RemoteException
         */
        public void goToContinuePlayPopup(String assetID) throws RemoteException;

        /**
         * 현재 재생중인 컨텐츠의 정보를 전달
         * Title, Actors, Directors ("," 구분자 사용)
         * @return
         * @throws RemoteException
         */
        public String[] getCurrentPlayingContentInfo() throws RemoteException;

        /**
         * VOD 모바일구매여부 변경시 알려줌
         *
         * @param isUsing
         *            사용여부
         * @throws RemoteException
         */
        public void notifyMobilePurchaseChanging(boolean isUsing) throws RemoteException;

        /**
         * "epg/popularVod.zip" 파일 변경시 알려줌
         *
         * @throws RemoteException
         */
        public void notifyPopularImageChanging() throws RemoteException;

        /**
         * VBM HitCount 전송 요청
         *
         * @throws RemoteException
         */
        public void requestSendingHitCount() throws RemoteException;

}

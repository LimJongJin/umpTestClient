package com.castis.util;

public class Logger {
    
    	public static final String UMP = "UMP";
	public static final int INFO = 0;

	public static final int WARNING = 1;

	public static final int ERROR = 2;

	public static final int FAIL = 3;

	public static boolean isLog = false;

	public static void println(Object object, String message) {
		println(INFO, object, message);
	}

	public static void println(Class klass, String message) {
		println(INFO, klass, message);
	}

	public static boolean isLog() {
		return isLog;
	}

	public static void setLog(boolean isLog) {
		Logger.isLog = isLog;
		CiUtils.setDebugMessage(isLog);
	}

	private static String getPrefix() {
		//return  "CASTIS,RVOD," + DateUtil.getDateString("yyyy-MM-dd") + "," + DateUtil.getDateString("HH:mm:ss.SSS") + ",";
	    return UMP + ",";
	}

	public static void println(int logType, Object object, String message) {

		if (!Logger.isLog() && logType == INFO)
			return;

		String name = object.getClass().getName();
		switch (logType) {
		case INFO: {
			System.out.println(getPrefix() + "INFO," + name + ",," + message);
			break;
		}
		case WARNING: {
			System.out.println(getPrefix() + "WARNING," + name + ",," + message);
			break;
		}
		case ERROR: {
			System.out.println(getPrefix() + "ERROR," + name + ",," + message);
			break;
		}
		case FAIL: {
			System.out.println(getPrefix() + "FAIL," + name + ",," + message);
			break;
		}
		}
		name = null;
	}

	public static void println(int logType, Class klass, String message) {
		if (!Logger.isLog() && logType == INFO)
			return;

		String name = klass.getName();
		switch (logType) {
		case INFO: {
			System.out.println(getPrefix() + "INFO," + name + ",," + message);
			break;
		}
		case WARNING: {
			System.out.println(getPrefix() + "WARNING," + name + ",," + message);
			break;
		}
		case ERROR: {
			System.out.println(getPrefix() + "ERROR," + name + ",," + message);
			break;
		}
		case FAIL: {
			System.out.println(getPrefix() + "FAIL," + name + ",," + message);
			break;
		}
		}
		name = null;

	}
}
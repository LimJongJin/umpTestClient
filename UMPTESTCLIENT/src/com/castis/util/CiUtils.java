///*
// * Created on 2004. 2. 24.
// *
// * To change the template for this generated file go to
// * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
// */
//package com.castis.vod.utils;
package com.castis.util;
//
////removed by core 2005. 8. 7	
////
//import java.io.DataInputStream;
//import java.io.DataOutputStream;
//import java.io.IOException;
//import java.sql.Timestamp;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;
import java.util.StringTokenizer;

//
//
//
///**
// * @author sasgas
// *
// * To change the template for this generated type comment go to
// * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
// */
public class CiUtils {
//	// written by jyjung. 04.7.15 
//	public static int Ci_Bool_t ;
//	public static final int TRUE = 1;
//	public static final int FALSE = 0;
//	//
//	
//
//	static public SimpleDateFormat formatter = null;
//	static public Calendar cal = null;
//	static public String today = null;
//	static public Timestamp ts = null;
//
//	 removed by core 2005. 8. 7	
//	
//	public static final int MAX_FILE_PATH_LENGTH = 256;
//	public static final int MAX_FILE_NAME_LENGTH = MAX_FILE_PATH_LENGTH;
//	public static final int MAX_IP_ADDRESS_LENGTH = 32;
//	public static final int MAX_NAME_LENGTH = 64;
//	public static final int MAX_DESCRIPTION_LENGTH = 256;
//	public static final int USER_ID_LENGTH = 24+1;
//	public static final int TIME_LENGTH = 14+1;
//
//	public static final String CI_DIRECTORY_DELIMITER = "~=";
//	public static final String DIRECTORY_DELIMITER = "/";
	
	private static boolean m_debugMessage = true;
	
	public CiUtils() {
//		formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//		cal = Calendar.getInstance();
	}
//	static public Timestamp GetCurrentTime() {
//		today = formatter.format(cal.getTime());
//		ts = Timestamp.valueOf(today);
//
//		return ts;
//	}
	static public String fillPrefixByZero(String szSource, int fixLength) {
		StringBuffer szTemp = new StringBuffer();
		int iLength = fixLength - szSource.length();
		if ( iLength > 0 ) {
			for( ; iLength > 0 ; iLength-- ) {
				szTemp.append("0");
			}
			szTemp.append(szSource);
			return szTemp.toString();
		}
		Nullizer.Nullize(szTemp);
		return szSource; 
	}
	static public String NullStringTrim(String szSource) {
		StringTokenizer st = new StringTokenizer(szSource, "\0");
		szSource = st.nextToken();
		Nullizer.Nullize(st);

		return szSource;
	}
	static public void CiLogger(String message) {
		if (message != null && m_debugMessage == true) {
			System.out.println(message + System.currentTimeMillis());
		}
	}
	static public void setDebugMessage(boolean isDebug) {
		if (isDebug == true || isDebug == false) {
			m_debugMessage = isDebug;
		}
	}
//	public void finalize() {
//		formatter = null;
//	}
//	public static String readString(DataInputStream in, int max)
//		throws IOException {
//		int length = in.readInt();
//		
//		if (length < 1)
//			length = 1;
//		
//		length = (length < max) ? length : max;
//		
//		byte[] byteString = new byte[length];
//		in.readFully(byteString);
//		String string = new String(byteString, 0, length-1, "EUC_KR");
//		//+KDMC JVM
//		byteString = null;
//		//-
//		return string;
////		return new String(string, "EUC_KR").trim();
//	}
//	
//	public static String readFixedString(DataInputStream in, int length)
//		throws IOException {
//		byte[] byteString = new byte[length];
//		in.readFully(byteString);
//		
//		int index = 0;
//		for (; index < length; index++) {
//			if (byteString[index] == (byte)0)
//				break;
//		}
//		String string = new String(byteString, 0, index, "EUC_KR");
//		//+KDMC JVM
//		byteString = null;
//		//-
//		return string ;
//	}
//	
//	public static void writeString(DataOutputStream out, String string)
//		throws IOException {
//		byte[] bytes = string.getBytes("EUC_KR");
//		
//		out.writeInt(bytes.length + 1);
//		out.write(bytes);
//		out.writeByte(0);
//		//+KDMC JVM
//		bytes = null;
//		//-
//	}
//	
//	public static void writeFixedString(DataOutputStream out, String string, int length)
//		throws IOException {
//		byte[] srcBytes = string.getBytes("EUC_KR");
//		
//		byte[] destBytes = new byte[length];
//		Arrays.fill(destBytes, (byte)0);
//		
//		length = (length < srcBytes.length) ? length : srcBytes.length;
//		
//		System.arraycopy(srcBytes, 0, destBytes, 0, length);
//		
//		out.write(destBytes);
//	}
//	
//	
//	 removed by core 2005. 8. 7	
//	
//	public static int sizeof(int i) {
//		return 4;
//	}
//	
//	public static int sizeof(long l) {
//		return 8;
//	}
//	
//	public static int sizeof(String string)
//		throws IOException {
//		return 4									// length
//				+ string.getBytes("EUC_KR").length	// string
//				+ 1;								// null-character
//	}
}

package com.castis.vod;

/**
 * GenericVODClient가 VOD 및 SRM Server와 통신한 결과값입니다.
 */
public class VodClientResult_enum {
	/**
	 *  해당 명령 성공
	 */
	public static final int VODCLIENT_SUCCESS 									= 200;
	
	/**
	 *  유효하지 않은 명령, 해당 명령은 VOD 서버에서 무시됨
	 */
	public static final int VODCLIENT_CONTINUE 									= 300;

	/**
	 *  VOD 서버 에러
	 */
	public static final int VODCLIENT_SERVER_ERROR 								= 10000;
	/**
	 * RTSP 메시지를 VOD 서버에서 분석할 수 없음, 잘못된 RTSP 메시지
	 */
	public static final int VODCLIENT_SEMANTIC_ERROR 							= 10001;
	/**
	 * Open()시 입력값이 빠져있거나 적절하지 않은 값
	 */
	public static final int VODCLIENT_SEMANTIC_INVALID_ARGUMENT					= 10002;
	/**
	 * 셋업 도중 SRM 서버와 VOD 서버간 통신 에러
	 */
	public static final int VODCLIENT_CONNECT_RESULT_SERVER_ERROR 				= 10003;
	/**
	 * 셋업 도중 LB와의 통신, 또는 SRM 내부적인 문제 발생
	 */
	public static final int VODCLIENT_CONNECT_RESULT_SRM_SERVER_ERROR			= 10004;
	/**
	 * 리셋업도중 타이머 스케줄링 에러 
	 */
	public static final int VODCLIENT_CANNOT_SCHEDULE_TIMER						= 10005;

	/**
	 * SRM 서버에 셋업을 하기 위해 연결을 시도했으나 실패
	 */
	public static final int VODCLIENT_ERROR_CANNOT_CONNECT_SRM_TO_SETUP 		= 20000;
	/**
	 * SRM 서버에 남아있는 자원을 해제하기 위해 연결을 시도했으나 실패
	 */
	public static final int VODCLIENT_ERROR_CANNOT_CONNECT_SRM_TO_CLEAN			= 20001;
	/**
	 * SRM 서버에 DSMCC 셋업 보내기 실패
	 */
	public static final int VODCLIENT_ERROR_DSMCC_SEND_SETUP					= 20010;
	/**
	 * SRM 서버에 DSMCC 셋업을 보낸 후 그에 대한 결과를 받는데 실패
	 */
	public static final int VODCLIENT_ERROR_DSMCC_RECV_SETUP					= 20011;
	/**
	 * SRM 서버에 자원을 해제하기 위해 DSMCC 릴리즈 요청을 보내려 했으나 실패
	 */
	public static final int VODCLIENT_ERROR_DSMCC_SEND_RELEASE					= 20020;
	/**
	 * SRM 서버에 자원을 해제하기 위해 DSMCC 릴리즈 요청을 보낸 후 그 결과를 받는데 실패
	 */
	public static final int VODCLIENT_ERROR_DSMCC_RECV_RELEASE					= 20021;
	/**
	 * 최대 연결 세션을 초과했다고 SRM 서버에서 알려줌
	 */
	public static final int VODCLIENT_ERROR_SRM_MAXSESSION_ERROR 				= 20100;
	/**
	 * 요청한 파일이 존재하지 않거나 유효하지 않은 파일
	 */
	public static final int VODCLIENT_ERROR_DSMCC_INIT_IO_ERROR 				= 20101;
	/**
	 * QAM에 가용한 자원이 없음, 서비스하기 위한 네트워크 대역폭이 꽉 참
	 */
	public static final int VODCLIENT_ERROR_NOT_ENOUGH_NETWORK_RESOURCE 		= 20102;
	/**
	 * SRM 서버에서 클라이언트가 설정한 TSID 를 찾을 수 없음
	 */
	public static final int VODCLIENT_ERROR_INVALID_TSID 						= 20103;
	/**
	 * SRM은 동작중이나, Setup 요청을 거부함. (DB rescan등의 경우에)
	 */
	public static final int VODCLIENT_ERROR_SETUP_REJECTED 						= 20104;
	/**
	 * SRM 서버로 DSMCC 셋업을 하는 도중 발생한 알 수 없는 에러
	 */
	public static final int VODCLIENT_ERROR_DSMCC_RESULT_UNKNOWN_ERROR			= 29999;

	/**
	 * VOD 서버로 연결을 시도하기 전 필요한 정보가 설정되지 않음
	 * 서버 주소, 서버 포트번호, 파일 이름, 서비스 대상 목적지 주소, 서비스 대상 목적지 포트번호
	 */
	public static final int VODCLIENT_ERROR_INVALID_URL_FORMAT_ERROR 			= 30000;
	/**
	 * 클라이언트로부터 세션 셋업 요청을 받았으나, SRM 서버를 통해 준비하라는 요청을 받지 않은 세션임
	 */
	public static final int VODCLIENT_ERROR_NO_PREPARED_SESSION					= 30001;
	/**
	 * 셋업 명령을 수행할 세션정보가 존재하지 않음
	 */
	public static final int VODCLIENT_ERROR_SESSION_NOT_FOUND					= 30002;
	/**
	 * VOD 서버에서 서비스 가능한 최대 대역폭을 초과함
	 */
	public static final int VODCLIENT_ERROR_MAX_BANDWIDTH_ERROR 				= 30003;
	/**
	 * VOD 서버에 Describe를 하고자 연결을 시도했으나 실패
	 */ 
	public static final int VODCLIENT_ERROR_CANNOT_DESCRIBE_VODSERVER			= 30010;
	/**
	 * Describe시 파일이 존재하지 않거나 유효하지 않은 파일 이라고 알려줌
	 */
	public static final int VODCLIENT_ERROR_DESCRIBE_INIT_IO_ERROR 				= 30011;
	/**
	 * Describe시 VOD 서버에서 서비스 가능한 최대 연결 세션을 초과했다고 알려줌
	 */
	public static final int VODCLIENT_ERROR_DESCRIBE_MAXSESSION_ERROR			= 30012;
	/**
	 * Describe시 VOD 서버에서 서비스 가능한 최대 대역폭을 초과했다고 알려줌
	 */
	public static final int VODCLIENT_ERROR_DESCRIBE_MAX_BANDWIDTH_ERROR		= 30013;
	/**
	 * VOD 서버에 셋업을 하기 위해 연결을 시도했으나 실패
	 */
	public static final int VODCLIENT_ERROR_CANNOT_SETUP_VODSERVER				= 30020;
	/**
	 * Setup시 파일이 존재하지 않거나 유효하지 않은 파일이라고 알려줌
	 */
	public static final int VODCLIENT_ERROR_SETUP_INIT_IO_ERROR					= 30021;
	/**
	 * VOD 서버에서 서비스 가능한 최대 연결 세션을 초과
	 */
	public static final int VODCLIENT_ERROR_MAXSESSION_ERROR					= 30022;
	/**
	 * VOD 서버에 더이상 서비스 가능한 네트워크 대역이 없음
	 */
	public static final int VODCLIENT_ERROR_NOT_ENOUGH_BANDWIDTH				= 30023;
	/**
	 * VOD 서버로부터 Setup 명령에 대한 처리 결과를 받았으나 내용이 없음
	 */
	public static final int VODCLIENT_ERROR_DSMCC_RECV_SETUP_RESPONSE_NULL		= 30024;
	/**
	 * 라이센스 등의 이유로 지원되지 않는 미디어 타입
	 * 예를 들어 Mpeg4에 대한 라이센스는 발급되지 않았는데 서비스를 요청함
	 */
	public static final int VODCLIENT_ERROR_UNSUPPORTED_MEDIA_TYPE				= 30025;
	/**
	 * VOD 서버에서 지원되지 않는 전송 타입
	 * 예를 들어 ISMA가 지원이 되지 않는데 서비스를 요청함
	 */
	public static final int VODCLIENT_ERROR_UNSUPPORTED_TRANSPORT				= 30026;
	/**
	 * VOD 서버가 비활성화 상태
	 */
	public static final int VODCLIENT_ERROR_VOD_SERVER_INACTIVE					= 30027;
	/**
	 * 서버와 클라이언트 간에 주기적으로 상태체크를 하기 위한 Report socket을 VOD 서버에 연결하고자 했으나 실패
	 */
	public static final int VODCLIENT_ERROR_CANNOT_CONNECT_REPORT_SOCKET 		= 30304;
	/**
	 * Describe 명령에 대한 응답을 받는 과정에서 네트워크 오류 발생
	 */
	public static final int VODCLIENT_ERROR_RECV_DESCRIBE_SOCKET				= 31010;
	/**
	 * Setup 명령에 대한 응답을 받는 도중 네트워크 오류 발생
	 */
	public static final int VODCLIENT_ERROR_RECV_SETUP_SOCKET					= 31020;
	/**
	 * Teardown 명령에 대한 응답을 받는 도중 네트워크 오류 발생
	 */
	public static final int VODCLIENT_ERROR_RECV_TEARDOWN_SOCKET				= 31040;
	/**
	 * Play 명령에 대한 응답을 받는 도중 네트워크 오류 발생
	 */
	public static final int VODCLIENT_ERROR_RECV_PLAY_SOCKET					= 31050;
	/**
	 * Pause 명령에 대한 응답을 받는 도중 네트워크 오류 발생
	 */
	public static final int VODCLIENT_ERROR_RECV_PAUSE_SOCKET					= 31060;

//	/**
//	 * VOD 서버로 RTSP 명령을 주고 받기 위한 연결을 하려 했으나 초기화에 실패
//	 */
//	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_CONNECT			= 40000;
	/**
	 * VOD 서버로 RTSP Describe 명령을 보내려 했으나 초기화 실패
	 */
	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_DESCRIBE			= 40010;
	/**
	 * VOD 서버로 RTSP Describe 명령을 보냈으나 VOD 서버에서 분석하지 못함
	 * 적절하지 않은 Describe 구문을 서버에 보냄
	 */
	public static final int RTSPCLIENT_ERROR_SEMANTIC_INVALID_DESCRIBE			= 40011;
	/**
	 * VOD 서버로 Describe 명령 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_DESCRIBE_ERROR				= 40012;
	/**
	 * VOD 서버로부터 Describe 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECV_DESCRIBE_ERROR 				= 40013;
	/**
	 * VOD 서버로부터 Describe 명령에 대한 처리 결과를 받았으나 알 수 없는 결과값
	 */
	public static final int RTSPCLIENT_ERROR_DESCRIBE_RESULT_UNKNOWN			= 40019;
	/**
	 * VOD 서버로 RTSP Setup 명령을 보내려 했으나 초기화 실패
	 */
	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_SETUP			= 40020;
	/**
	 * VOD 서버로 Setup 명령 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_SETUP_ERROR 					= 40022;
	/**
	 * VOD 서버로부터 Setup 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECV_SETUP_ERROR 					= 40023;
	/**
	 * VOD 서버로부터 Setup 명령에 대한 처리 결과를 받았으나 알 수 없는 결과값
	 */
	public static final int RTSPCLIENT_ERROR_SETUP_RESULT_UNKNOWN				= 40029;
	/**
	 * VOD 서버로 RTSP Set parameter 명령을 보내려 했으나 초기화 실패
	 */
	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_SET_PARAMETER	= 40030;
	/**
	 * VOD 서버로 Set smart card ID 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_SET_SMART_CARD_ID				= 40032;
	/**
	 * VOD 서버로부터 Ser smart card ID 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECV_SET_SMART_CARD_ID				= 40033;
	/**
	 * VOD 서버로 Set pin code 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_SET_PIN_CODE					= 40034;
	/**
	 * VOD 서버로부터 Set pin code 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECV_SET_PIN_CODE					= 40035;
	/**
	 * VOD 서버로 Set user ID 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_SET_USER_ID					= 40036;
	/**
	 * VOD 서버로부터 Set user ID 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECV_SET_USER_ID					= 40037;
	/**
	 * VOD 서버로 Set menu ID 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_SET_MENU_ID					= 40038;
	/**
	 * VOD 서버로부터 Set menu ID 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECV_SET_MENU_ID					= 40039;
	/**
	 * VOD 서버로 RTSP Teardown 명령을 보내려 했으나 초기화 실패
	 */
	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_TEARDOWN			= 40040;
	/**
	 * VOD 서버로 Teardown 명령 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_TEARDOWN 						= 40042;
	/**
	 * VOD 서버로부터 Teardown 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECEIVE_TEARDOWN 					= 40043;
	/**
	 * VOD 서버로부터 Teardown 명령에 대한 처리 결과를 받았으나 알 수 없는 결과값
	 */
	public static final int RTSPCLIENT_ERROR_RESULT_TEARDOWN 					= 40048;
	/**
	 * VOD 서버로 RTSP Play 명령을 보내려 했으나 초기화 실패
	 */
	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_PLAY				= 40050;
	/**
	 * VOD 서버에서 Play 요청에 대한 구문 분석 에러
	 */
	public static final int RTSPCLIENT_ERROR_PLAY_MSG_PARSING_ERROR				= 40051;
	/**
	 * VOD 서버로 Play 명령 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_PLAY 							= 40052;
	/**
	 * VOD 서버로부터 Play 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECEIVE_PLAY 						= 40053;
	/**
	 * Begin of session 이라서 Play 요청을 수행할 수 없음
	 */
	public static final int RTSPCLIENT_ERROR_SESSION_BOF						= 40054;
	/**
	 * End of session 이라서 Play 요청을 수행할 수 없음
	 */
	public static final int RTSPCLIENT_ERROR_SESSION_EOF						= 40055;
	/**
	 * VOD 서버에서 유효하지 않은 탐색 요청 값이라고 판단함
	 */
	public static final int RTSPCLIENT_ERROR_INVALID_SEEK						= 40056;
	/**
	 * VOD 서버로부터 Play 명령에 대한 처리 결과로 받은 값에 내용이 없음
	 */
	public static final int RTSPCLIENT_ERROR_RECEIVE_PLAY_RESPONSE_NULL 		= 40057;
	/**
	 * VOD 서버의 에러로 인해 RTSP Play 명령에 대한 처리 실패
	 */
	public static final int RTSPCLIENT_ERROR_RESULT_PLAY 						= 40058;
	/**
	 * VOD 서버에 RTSP Play 명령을 수행할 해당 세션이 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_PLAY_SESSION_NOT_FOUND				= 40059; 
	/**
	 * VOD 서버로 RTSP Pause 명령을 보내려 했으나 초기화 실패
	 */
	public static final int RTSPCLIENT_ERROR_NOT_INITIALIZE_TO_PAUSE			= 40060;
	/**
	 * VOD 서버로 Pause 명령 보내기 실패
	 */
	public static final int RTSPCLIENT_ERROR_SEND_PAUSE							= 40062;
	/**
	 * VOD 서버로부터 Pause 명령에 대한 처리 결과 받기 실패
	 */
	public static final int RTSPCLIENT_ERROR_RECEIVE_PAUSE 						= 40063;
	/**
	 * VOD 서버에서 Pause 명령에 대한 처리 실패
	 */
	public static final int RTSPCLIENT_ERROR_RESULT_PAUSE 						= 40068;
	/**
	 * 미디어 탐색 요청(Seek())시 처리 범위를 벗어난 값을 요청
	 */
	public static final int RTSPCLIENT_ERROR_INAVLID_SEEK_VALUE 				= 40100;
	/**
	 * 앞으로 감기 요청(FastForward())시 적절하지 않은 값을 요청
	 */
	public static final int RTSPCLIENT_ERROR_INAVLID_FF_VALUE 					= 40101;
	/**
	 * 뒤로 감기 요청(Rewind())시 적절하지 않은 값을 요청
	 */
	public static final int RTSPCLIENT_ERROR_INAVLID_RW_VALUE 					= 40102;
	/**
	 * 현재 세션에서는 인덱싱 선처리를 하지 않는 등의 이유로 트릭모드가 불가능한데 요청함
	 */
	public static final int RTSPCLIENT_ERROR_COULD_NOT_TRICKABLE 				= 40401;
	/**
	 * 유효하지 않은 Offset으로 탐색(Seek())을 요청함
	 */
	public static final int RTSPCLIENT_ERROR_INVALID_OFFSET 					= 40402;
	/**
	 * VOD 서버로 연결하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_CONNECT_SESSION_NULL				= 41000;
	/**
	 * VOD 서버로 Describe 명령을 하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_DESCRIBE_SESSION_NULL				= 41010;
	/**
	 * VOD 서버로 Setup 명령을 하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_SETUP_SESSION_NULL					= 41020;
	/**
	 * VOD 서버로 Set Parameter 명령을 하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_SET_PARAMETER_SESSION_NULL			= 41030;
	/**
	 * VOD 서버로 Teardown 명령을 하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_TEARDOWN_SESSION_NULL				= 41040;
	/**
	 * VOD 서버로 Play 명령을 하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_PLAY_SESSION_NULL					= 41050;
	/**
	 * VOD 서버로 Pause 명령을 하고자 했으나 필요한 세션 정보가 존재하지 않음
	 */
	public static final int RTSPCLIENT_ERROR_PAUSE_SESSION_NULL					= 41060;

	/**
	 * ReOpen 또는 Resetup의 결과 프로그램 번호가 바뀜
	 */
	public static final int VODCLIENT_REOPEN_PROGRAM_NUMBER_CHANGE 				= 20304;
	/**
	 * 알 수 없는 에러
	 */
	public static final int VODCLIENT_UNKNOWN_ERROR 							= 50000;
	
	/** 
	 * UMP error : sd2에서 정의.
	 */
	public static final int UMP_ERROR_RECEIVE_SESSIONADDRESS 				= 45013;
	
	public static final int UMP_ERROR_SOCKET_ERROR 						= 45100;
}
package com.castis.manager;

import com.alticast.navsuite.core.cj.CjSystemInfo;
import com.castis.util.Logger;

import java.util.HashMap;

public class STBInfoManager {

	public static STBInfoManager getInstance() {
		return instance;
	}

	private static final STBInfoManager instance = new STBInfoManager();

	public static final String VERSION = "UMP_1.0.1QR5";

	private SectionManager sectionManager;

	public STBInfoManager() {
		sectionManager = new SectionManager();
	}

	public int getMapId() {
		return this.sectionManager.getMapId();
	}

	public SectionManager getSectionManager() {
		return sectionManager;
	}

	public String getMacAddress() {
		byte[] mac = null;

		mac = CjSystemInfo.getHostMacAddress();
		StringBuffer macAddressBuffer = new StringBuffer();

//         Test용
//        String sMac = "4C:D0:8A:26:4E:C1";
//		mac = sMac.getBytes();

		for (int i = 0; i < mac.length; i++) {
			int prefix = (int) ((mac[i] >> 4) & 15);
			int postfix = (int) (mac[i] & 15);
			String preMAC = Integer.toHexString(prefix).toUpperCase();
			String postMAC = Integer.toHexString(postfix).toUpperCase();

			macAddressBuffer.append(preMAC);
			macAddressBuffer.append(postMAC);

			if (i < mac.length - 1)
				macAddressBuffer.append(":");
		}
		Logger.println(this, "Host Mac Address=" + macAddressBuffer.toString().trim());

		mac = null;

		return macAddressBuffer.toString().trim();
	}

	public String getSTBModelNumber() {
		String modelNumber = CjSystemInfo.getModelNumber();
		if (modelNumber == null)
			modelNumber = "null";

//         Test용
//        String modelNumber = "uc3300";

		return modelNumber;
	}

	private String pwsIp;

	private String pwsPort;

	private String terminalKey;

	private String srmIp;

	private int soId;

	public String getPwsIp() {
		return pwsIp;
	}

	public void setPwsIp(String pwsIp) {
		this.pwsIp = pwsIp;
	}

	public String getPwsPort() {
		return pwsPort;
	}

	public void setPwsPort(String pwsPort) {
		this.pwsPort = pwsPort;
	}

	public String getTerminalKey() {
		return terminalKey;
	}

	public void setTerminalKey(String terminalKey) {
		this.terminalKey = terminalKey;
	}

	private int stbId;

	public void setSTBID(int stbId) {
		this.stbId = stbId;
	}

	public int getStbId() {
		return this.stbId;
	}

	public void setSRMIP(String srmIp) {
		this.srmIp = srmIp;

	}

	public String getSRMIP() {
		return this.srmIp;
	}

	public void setSoID(int soId) {
		this.soId = soId;
	}

	public int getSOID() {
		return this.soId;
	}

	private HashMap pwsIpMap;

	public String getPWSIpByMapId(String mapId) {
		String pwsIp;
		if (pwsIpMap == null) {
			pwsIp = null;
		} else {
			if (pwsIpMap.containsKey(mapId)) {
				pwsIp = (String) pwsIpMap.get(mapId);
			} else {
				pwsIp = (String) pwsIpMap.get("default");
			}
		}
		return pwsIp;
	}

	public void setPwsIpMap(HashMap pwsIpMap) {
		this.pwsIpMap = pwsIpMap;
	}
}
